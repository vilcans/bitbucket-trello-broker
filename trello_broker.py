# Bitbucket broker for Trello

# Copyright (C) 2013 Goo Technologies AB
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import urllib
#from django.utils import simplejson as json
import json
import re

try:
    from brokers import BaseBroker
except ImportError:
    class BaseBroker():
         def get_local(self, arg, theclass):
             return theclass()


card_re = re.compile(r'#(\d{1,5})\b')


class URLOpener(urllib.FancyURLopener):
    version = 'bitbucket.org'


class Trello(BaseBroker):
    def handle(self, payload):
        key = payload['service']['key']
        token = payload['service']['token']
        board_id = payload['service']['board']

        opener = self.get_local('opener', URLOpener)

        def open(service, data=None):
            if isinstance(data, dict):
                data = urllib.urlencode(data)
            url = 'https://api.trello.com' + service + '?key=' + key + '&token=' + token
            stream = opener.open(url, data)
            if stream.code > 299:
                raw_data = stream.read()
                stream.close()
                raise RuntimeError('URL request failed: %s %s' % (stream.code, raw_data))
            return stream

        def add_comment(short_id, text):
            try:
                stream = open('/1/board/' + board_id + '/cards/' + str(short_id))
            except Exception as e:
                print 'Error getting card: %r - ignoring card %s' % (e, short_id)
                return
            card_data = json.load(stream)
            stream.close()

            long_id = card_data['id']

            try:
                open('/1/cards/' + long_id + '/actions/comments', {'text': text}).close()
            except Exception as e:
                print 'Error commenting: %r - ignoring card %s' % (e, short_id)
                return
            stream.close()

        for commit in payload['commits']:
            message = commit['message']
            ids = set(int(id) for id in card_re.findall(message))
            url = 'https://bitbucket.org' + payload['repository']['absolute_url'] + 'commits/' + commit['raw_node']
            comment = 'Commit %s by %s:\n%s\n%s' % (
                commit['node'], commit['author'], message, url
            )

            for id in ids:
                add_comment(id, comment)
