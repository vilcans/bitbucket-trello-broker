# Bitbucket Trello Broker

**NOTE:** This is designed to be installed and run on
Atlassian's Bitbucket servers,
something [they have no intent of doing](https://bitbucket.org/site/master/issue/6186/add-broker-for-trello).
It can be modified to use Bitbucket's
[POST hook](https://confluence.atlassian.com/display/BITBUCKET/POST+hook+management) instead
(which is what we have done at [our company](http://www.gooengine.com),
but that code is not public).

## Information

Whenever you push a commit to Bitbucket containing references to Trello cards,
the commit message is posted as a message on your Trello board.

A reference to a card is a `#` followed by the card's "short ID".
This ID is the one visible when you click the card
(in the lower right corner).

Required service parameters:

*key*: Your API key. Get it at https://trello.com/1/appKey/generate

*token*: An API token. Get it at
`https://trello.com/1/connect?key=KEY&name=Bitbucket%20Technologies&expiration=never&response_type=token&scope=read,write`
(replacing <KEY> with your API key).

*board*: ID of the board to use.
This is the last part of the URL when you're visiting a board on Trello.
For example the board ID for the board at `https://trello.com/board/trello-development/4d5ea62fd76aa1136000000c`
is `4d5ea62fd76aa1136000000c`.
